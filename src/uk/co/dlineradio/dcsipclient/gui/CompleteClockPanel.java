package uk.co.dlineradio.dcsipclient.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;

import uk.co.dlineradio.dcsipclient.settings.ClockSettings;

/**
 * A complete analogue and digital clock panel.
 * @author Marc Steele
 */

public class CompleteClockPanel extends JPanel implements Runnable {
	
	private boolean runThread = false;
	private Thread runningThread = null;
	private JLabel timePanel = null;
	private JLabel datePanel = null;
	AnalogueClockPanel analogueClock = null;
	
	private static Logger logger = Logger.getLogger(CompleteClockPanel.class.getName());
	private static final int SLEEP_TIME = 500;
	
	/**
	 * Creates a new instance of the panel.
	 */
	
	private CompleteClockPanel(ClockSettings settings) {
		
		super();
		
		// Setup the parent panel
		
		this.setLayout(new BorderLayout());
		this.setOpaque(false);
		
		// Let's create the font
		
		Font clockFont = new Font(settings.getFontName(), Font.BOLD, settings.getFontSize());
		
		// The text clock bits
		
		this.timePanel = new JLabel();
		this.datePanel = new JLabel();
		
		this.timePanel.setFont(clockFont);
		this.datePanel.setFont(clockFont);
		
		this.timePanel.setForeground(settings.getFontColour());
		this.datePanel.setForeground(settings.getFontColour());
		
		this.timePanel.setHorizontalAlignment(JLabel.CENTER);
		this.datePanel.setHorizontalAlignment(JLabel.CENTER);
		
		this.add(this.timePanel, BorderLayout.SOUTH);
		this.add(this.datePanel, BorderLayout.NORTH);
		
		// The analogue clock
		
		this.analogueClock = AnalogueClockPanel.create(settings);
		
		if (this.analogueClock != null) {
			this.add(this.analogueClock, BorderLayout.CENTER);
		} else {
			logger.log(Level.WARNING, "Ran into a problem generating the analogue clock. I guess we'd better show nothing.");
		}
		
		
		// Launch the thread
		
		this.startThread();
		
	}
	
	/**
	 * Attempts to create a new instance of the clock panel.
	 * @param settings The clock settings we'll be using.
	 * @return The clock panel if we could create it. Otherwise null.
	 */
	
	public static CompleteClockPanel get(ClockSettings settings) {
		
		if (settings == null) {
			logger.log(Level.WARNING, "Asked to create a new clock panel but not supplied the required settings.");
			return null;
		} else {
			return new CompleteClockPanel(settings);
		}
		
	}

	@Override
	public void run() {
		
		while (this.runThread) {
			
			// Update the date and time
			
			Calendar now = Calendar.getInstance();
			this.datePanel.setText(String.format("%1$TA, %1$Te %1$TB %1$TY", now));
			this.timePanel.setText(String.format("%1$TT", now));
			
			// Poke the analogue clock
			
			if (this.analogueClock != null) {
				this.analogueClock.repaint();
			}
			
			// Sleep it off
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "Clock panel thread interrupted.", e);
			}
			
		}
		
	}
	
	/**
	 * Starts the thread if it's not already ticking along.
	 */
	
	public void startThread() {
		
		if (!this.runThread && this.runningThread == null) {
			this.runThread = true;
			this.runningThread = new Thread(this);
			this.runningThread.start();
		}
		
	}
	
	/**
	 * Stops the running thread (if there is one).
	 */
	
	public void stopThread() {
	
		if (this.runThread && this.runningThread != null) {
			this.runThread = false;
			this.runningThread.interrupt();
			this.runningThread = null;
		}
		
	}

}
