package uk.co.dlineradio.dcsipclient.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;

import uk.co.dlineradio.dcsipclient.settings.ClockSettings;

/**
 * JPanel for displaying a clock onscreen.
 * @author Marc Steele
 */

public class AnalogueClockPanel extends JPanel {
	
	private ClockSettings settings = null;
	private Color[] gradientColours = null;
	
	private static Logger logger = Logger.getLogger(AnalogueClockPanel.class.getName());
	private static final float[] GRADIENT_DISTRIBUTION = {0.3f, 1.0f};
	private static final double INTERVAL_TICK_LARGE = Math.PI / 6;
	private static final double INTERVAL_TICK_SMALL = Math.PI / 30;
	private static final double OFFSET_ANGLE = Math.PI / -2;
	
	/**
	 * Creates a new instance of the panel.
	 * @param settings The settings for drawing the clock.
	 */
	
	private AnalogueClockPanel(ClockSettings settings) {
		
		// Basics
		
		super();
		this.setOpaque(false);
		this.settings = settings;
		
		// Sort out the graident colours
		
		this.gradientColours = new Color[] {this.settings.getBackgroundColour(), this.settings.getBackgroundGradientColour()};
		
	}
	
	/**
	 * Attempts to create a new instance of the panel.
	 * @param settings The settings, if we could find them.
	 * @return The clock panel if all went well. Otherwise NULL.
	 */
	
	public static AnalogueClockPanel create(ClockSettings settings) {
		
		if (settings == null) {
			logger.log(Level.SEVERE, "Asked to create a clock panel but we were not given the settings required.");
			return null;
		} else {
			return new AnalogueClockPanel(settings);
		}
		
	}

	@Override
	public void paint(Graphics g) {
		
		super.paint(g);
		
		// Work out some basics
		
		Graphics2D g2 = (Graphics2D) g;
		int width = this.getWidth();
		int height = this.getHeight();
		Point centre = new Point(width / 2, height / 2);
		int clockSize = Math.min(width, height);
		int clockRadius = clockSize / 2;
		
		// Sanity check
		
		if (clockSize <= 0) {
			logger.log(Level.WARNING, "Cannot draw the clock as we don't have a valid drawing area.");
			return;
		}
		
		// Enable anti-aliasing
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		// Draw the clock background
		
		RadialGradientPaint clockBackgroundPaint = new RadialGradientPaint(centre, clockRadius, GRADIENT_DISTRIBUTION, this.gradientColours);
		g2.setPaint(clockBackgroundPaint);
		g2.fillOval(centre.x - clockRadius, centre.y - clockRadius, clockSize, clockSize);
		
		// Draw the big ticks
		
		g2.setPaint(this.settings.getTickColor());
		g2.setStroke(new BasicStroke(this.settings.getLargeTickWith()));
		
		for (int i = 0; i < 12; i++) {
			
			double angle = i * INTERVAL_TICK_LARGE;
			Point endPoint = new Point(
					(int) (centre.x + ((clockRadius - this.settings.getBorderSize()) * Math.sin(angle))),
					(int) (centre.y + ((clockRadius - this.settings.getBorderSize()) * Math.cos(angle)))
			);
			Point startPoint = new Point(
						(int) (centre.x + ((clockRadius - this.settings.getBorderSize() - this.settings.getLargeTickLength()) * Math.sin(angle))),
						(int) (centre.y + ((clockRadius - this.settings.getBorderSize() - this.settings.getLargeTickLength()) * Math.cos(angle)))
			);
			
			g2.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
			
		}
		
		// Now our little tick bretherin
		
		g2.setPaint(this.settings.getTickColor());
		g2.setStroke(new BasicStroke(this.settings.getSmallTickWidth()));
		
		for (int i = 0; i < 60; i++) {
			
			if (i % 5 != 0) {
			
				double angle = i * INTERVAL_TICK_SMALL;
				Point endPoint = new Point(
						(int) (centre.x + ((clockRadius - this.settings.getBorderSize()) * Math.sin(angle))),
						(int) (centre.y + ((clockRadius - this.settings.getBorderSize()) * Math.cos(angle)))
				);
				Point startPoint = new Point(
						(int) (centre.x + ((clockRadius - this.settings.getBorderSize() - this.settings.getSmallTickLength()) * Math.sin(angle))),
						(int) (centre.y + ((clockRadius - this.settings.getBorderSize() - this.settings.getSmallTickLength()) * Math.cos(angle)))
				);
			
				g2.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
				
			}
			
		}
		
		// And now onto the hands
		
		Calendar now = Calendar.getInstance();
		double hourPlusFraction = now.get(Calendar.HOUR) + (now.get(Calendar.MINUTE) / 60.0);
		double minutesPlusFraction = now.get(Calendar.MINUTE) + (now.get(Calendar.SECOND) / 60.0);
		double seconds = now.get(Calendar.SECOND);
				
		this.drawHand((float) (hourPlusFraction / 6.0 * Math.PI), centre, clockRadius - (2 * this.settings.getLargeTickLength()), (int) (clockRadius * 0.2), this.settings.getHoursHandWidth(), this.settings.getHoursHandColor(), g2);
		this.drawHand((float) (minutesPlusFraction / 30.0 * Math.PI), centre, clockRadius - this.settings.getLargeTickLength(), (int) (clockRadius * 0.2), this.settings.getMinutesHandWidth(), this.settings.getMinutesHandColor(), g2);
		this.drawHand((float) (seconds / 30.0 * Math.PI), centre, clockRadius - this.settings.getSmallTickLength(), (int) (clockRadius * 0.2), this.settings.getSecondsHandWidth(), this.settings.getSecondsHandColor(), g2);
		
		// And now the dot to cover
		
		g2.setColor(this.settings.getSecondsHandColor());
		g2.fillOval(centre.x - this.settings.getCentreDotRadius(), centre.y - this.settings.getCentreDotRadius(), this.settings.getCentreDotRadius() * 2, this.settings.getCentreDotRadius() * 2);
		
	}
	
	/**
	 * Draws a clock hand on the screen.
	 * @param angle The angle the hand will be at.
	 * @param centre The centre point on the clock.
	 * @param length The length of the hand.
	 * @param altLength The length of the hand that goes <b>over</b> the centre.
	 * @param width The width of the hand.
	 * @param colour The colour of the hand.
	 * @param g2 The 2D graphics system.
	 */
	
	private void drawHand(float angle, Point centre, int length, int altLength, int width, Color colour, Graphics2D g2) {
		
		// Work out the end point
		
		float altAngle = angle * -1;
		angle = (float) (altAngle + Math.PI);
		
		Point startPoint = new Point(
				(int) (centre.x + ((altLength - this.settings.getBorderSize()) * Math.sin(altAngle))),
				(int) (centre.y + ((altLength - this.settings.getBorderSize()) * Math.cos(altAngle)))
		);
		
		Point endPoint = new Point(
				(int) (centre.x + ((length - this.settings.getBorderSize()) * Math.sin(angle))),
				(int) (centre.y + ((length - this.settings.getBorderSize()) * Math.cos(angle)))
		);
		
		// Draw the line
		
		g2.setPaint(colour);
		g2.setStroke(new BasicStroke(width));
		g2.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
		
	}

}
