package uk.co.dlineradio.dcsipclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;

import uk.co.dlineradio.dcsipclient.settings.ClockSettings;

/**
 * Window for testing GUI compontents.
 * @author Marc Steele
 */

public class TestWindow extends JFrame {
	
	public TestWindow() {
		
		// Setup the window
		
		super("Test Window");
		this.setSize(800, 600);
		this.getContentPane().setBackground(Color.BLACK);
		
		// Add the components
		
		this.setLayout(new BorderLayout());
		
		CompleteClockPanel clock = CompleteClockPanel.get(new ClockSettings());
		this.add(clock, BorderLayout.CENTER);
		clock.startThread();
		
		// Show it
		
		this.setVisible(true);
		
	}

}
