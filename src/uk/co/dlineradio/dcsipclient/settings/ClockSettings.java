package uk.co.dlineradio.dcsipclient.settings;

import java.awt.Color;

/**
 * Holds the settings for the clock.
 * @author Marc Steele
 */

public class ClockSettings {
	
	private Color backgroundColour = Color.WHITE;
	private Color backgroundGradientColour = Color.LIGHT_GRAY;
	private Color tickColor = Color.BLACK;
	private Color secondsHandColor = Color.RED;
	private Color minutesHandColor = Color.BLACK;
	private Color hoursHandColor = Color.BLACK;
	private int secondsHandWidth = 5;
	private int minutesHandWidth = 7;
	private int hoursHandWidth = 12;
	private int smallTickWidth = 5;
	private int largeTickWith = 10;
	private int smallTickLength = 20;
	private int largeTickLength = 40;
	private int centreDotRadius = 15;
	private int borderSize = 10;
	private int fontSize = 64;
	private String fontName = "SansSerif";
	private Color fontColour = Color.WHITE;
	
	/**
	 * Obtains the main background colour of the clock.
	 * @return The main background colour.
	 */
	
	public Color getBackgroundColour() {
		return backgroundColour;
	}
	
	/**
	 * Sets the main background colour of the clock.
	 * @param backgroundColour The main background colour.
	 */
	
	public void setBackgroundColour(Color backgroundColour) {
		this.backgroundColour = backgroundColour;
	}
	
	/**
	 * Obtains the gradient background colour of the clock.
	 * @return The gradient background colour.
	 */
	
	public Color getBackgroundGradientColour() {
		return backgroundGradientColour;
	}
	
	/**
	 * Sets the gradient background colour of the clock.
	 * @param backgroundGradientColour The gradient background colour.
	 */
	
	public void setBackgroundGradientColour(Color backgroundGradientColour) {
		this.backgroundGradientColour = backgroundGradientColour;
	}
	
	/**
	 * Obtains the colour the ticks should be on the clock. 
	 * @return The colour of the ticks.
	 */
	
	public Color getTickColor() {
		return tickColor;
	}
	
	/**
	 * Sets the colour the ticks should be on the clock.
	 * @param tickColor The colour of the ticks.
	 */
	
	public void setTickColor(Color tickColor) {
		this.tickColor = tickColor;
	}
	
	/**
	 * Obtains the colour the seconds hand should be on the clock.
	 * @return The colour of the seconds hand.
	 */
	
	public Color getSecondsHandColor() {
		return secondsHandColor;
	}
	
	/**
	 * Sets the colour the seconds hand should be on the clock.
	 * @param secondsHandColor The colour of the seconds hand.
	 */
	
	public void setSecondsHandColor(Color secondsHandColor) {
		this.secondsHandColor = secondsHandColor;
	}
	
	/**
	 * Obtains the colour the minutes hand should be on the clock.
	 * @return The colour of the minutes hand.
	 */
	
	public Color getMinutesHandColor() {
		return minutesHandColor;
	}
	
	/**
	 * Sets the colour the minutes hand should be on the clock.
	 * @param minutesHandColor The colour of the minutes hand.
	 */
	
	public void setMinutesHandColor(Color minutesHandColor) {
		this.minutesHandColor = minutesHandColor;
	}
	
	/**
	 * Obtains the colour the hours hand should be on the clock.
	 * @return The colour of the minutes hand.
	 */
	
	public Color getHoursHandColor() {
		return hoursHandColor;
	}
	
	/**
	 * Sets the colour the hours hand should be on the clock.
	 * @param hoursHandColor The colour of the hours hand.
	 */
	
	public void setHoursHandColor(Color hoursHandColor) {
		this.hoursHandColor = hoursHandColor;
	}
	
	/**
	 * Obtains the width of the seconds hand.
	 * @return The width in pixels.
	 */
	
	public int getSecondsHandWidth() {
		return secondsHandWidth;
	}
	
	/**
	 * Sets the width of the seconds hand.
	 * @param secondsHandWidth The width in pixels.
	 */
	
	public void setSecondsHandWidth(int secondsHandWidth) {
		this.secondsHandWidth = secondsHandWidth;
	}
	
	/**
	 * Obtains the width of the minutes hand.
	 * @return The width in pixels.
	 */
	
	public int getMinutesHandWidth() {
		return minutesHandWidth;
	}
	
	/**
	 * Sets the width of the minutes hand.
	 * @param minutesHandWidth The width in pixels.
	 */
	
	public void setMinutesHandWidth(int minutesHandWidth) {
		this.minutesHandWidth = minutesHandWidth;
	}
	
	/**
	 * Obtains the width of the hours hand.
	 * @return The width in pixels.
	 */
	
	public int getHoursHandWidth() {
		return hoursHandWidth;
	}
	
	/**
	 * Sets the width of the hours hand.
	 * @param hoursHandWidth The width in pixels.
	 */
	
	public void setHoursHandWidth(int hoursHandWidth) {
		this.hoursHandWidth = hoursHandWidth;
	}
	
	/**
	 * Obtains the width of the small ticks.
	 * @return The width in pixels.
	 */
	
	public int getSmallTickWidth() {
		return smallTickWidth;
	}
	
	/**
	 * Sets the width of the small ticks.
	 * @param smallTickWidth The width in pixels.
	 */
	
	public void setSmallTickWidth(int smallTickWidth) {
		this.smallTickWidth = smallTickWidth;
	}
	
	/**
	 * Obtains the width of the large ticks.
	 * @return The width in pixels.
	 */
	
	public int getLargeTickWith() {
		return largeTickWith;
	}
	
	/**
	 * Sets the width of the large pixels.
	 * @param largeTickWith The width in pixels.
	 */
	
	public void setLargeTickWith(int largeTickWith) {
		this.largeTickWith = largeTickWith;
	}
	
	/**
	 * Obtains the length of the small ticks.
	 * @return The length in pixels.
	 */
	
	public int getSmallTickLength() {
		return smallTickLength;
	}
	
	/**
	 * Sets the length of the small ticks.
	 * @param smallTickLength The length in pixels.
	 */
	
	public void setSmallTickLength(int smallTickLength) {
		this.smallTickLength = smallTickLength;
	}
	
	/**
	 * Obtains the length of the large ticks.
	 * @return The length in pixels.
	 */
	
	public int getLargeTickLength() {
		return largeTickLength;
	}
	
	/**
	 * Sets the length of the large ticks.
	 * @param largeTickLength The length in pixels.
	 */
	
	public void setLargeTickLength(int largeTickLength) {
		this.largeTickLength = largeTickLength;
	}
	
	/**
	 * Obtains the radius of the centre dot.
	 * @return The radius in pixels.
	 */

	public int getCentreDotRadius() {
		return centreDotRadius;
	}
	
	/**
	 * Sets the radius of the centre dot.
	 * @param centreDotRadius The radius in pixels.
	 */

	public void setCentreDotRadius(int centreDotRadius) {
		this.centreDotRadius = centreDotRadius;
	}
	
	/**
	 * Obtains the size of the border on the clock.
	 * @return The size in pixels. 
	 */

	public int getBorderSize() {
		return borderSize;
	}
	
	/**
	 * Sets the size of the border on the clock.
	 * @param borderSize The size in pixels.
	 */

	public void setBorderSize(int borderSize) {
		this.borderSize = borderSize;
	}
	
	/**
	 * Obtains the size of the font on the text time/date.
	 * @return The size of the font.
	 */

	public int getFontSize() {
		return fontSize;
	}
	
	/**
	 * Sets the size of the fonr on the text time/date.
	 * @param fontSize The size of the font.
	 */

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	
	/**
	 * Obtains the name of the font on the text date/time.
	 * @return The name of the font.
	 */

	public String getFontName() {
		return fontName;
	}
	
	/**
	 * Sets the name of the font on the text date/time.
	 * @param fontName The name of the font.
	 */

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	
	/**
	 * Obtains the colour of the font on the text date/time.
	 * @return The colour of the font.
	 */

	public Color getFontColour() {
		return fontColour;
	}

	/**
	 * Sets the colour of the font on the text date/time.
	 * @param fontColour The colour of the font.
	 */
	
	public void setFontColour(Color fontColour) {
		this.fontColour = fontColour;
	}

}
