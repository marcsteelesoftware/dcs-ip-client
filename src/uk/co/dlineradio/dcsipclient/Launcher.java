package uk.co.dlineradio.dcsipclient;

import uk.co.dlineradio.dcsipclient.gui.TestWindow;

/**
 * Launches the application.
 * @author Marc Steele
 */

public class Launcher {

	/**
	 * Launches the application.
	 * @param args The command line arguments.
	 */
	
	public static void main(String[] args) {
		
		// Show the test window
		
		TestWindow test = new TestWindow();

	}

}
